%\documentclass[altfont]{uiophd}
%\usepackage{setspace} % setting line spacing
%\usepackage{parskip}  % space between paragraphs
%\usepackage{natbib} % bibliography stuff
%\usepackage{fixltx2e} % sub- and superscripts

%\begin{document}

% sets line spacing
%\setlength{\parindent}{20pt}
%\renewcommand\baselinestretch{0.1}
%\baselineskip=18pt plus1pt

% Own formulas ---------------------------------------------------------
%\newcommand{\degree}{\ensuremath{^\circ}} % degrees symbol

% Thesis introduction --------------------------------------------------
\chapter{Introduction} 
% ----------------------------------------------------------------------
Kelps, seaweeds and sea grasses provide the foundation for important ecosystem services, like food-web production processes that results in making goods available for exploitation. The macrophyte communities have therefore been, and are still, of considerable economic and cultural value for many societies. Recently, large-scale loss of these marine communities has become a widespread concern, and the contemporary scientific debate has received contributions form several corners of the world \citep[see e.g.][]{Muller2009, Waycott2009, Wernberg2011a, Nystrom2012, Harley2012}.
% Søren: Ville ha litt mer futt og fart her. Ikke fengende nok.

The fundamental ecological niches of macrophytes largely depend on the species' requirements for light, nutrients and substrate, and its tolerance-limits in relation to temperature and salinity. Within these boundaries, the distributions are also limited by interspecific competition and grazing.   
For decades, sea urchin grazing was considered the strongest driver of kelp deforestation \cite{Steneck2002}. Although grazing remains influential, the recent focus has shifted towards environmental change and pollution, as the impacts of these factors on the rest of the biosphere (including grazers) have become increasingly evident. The list of drivers responsible for kelp losses now includes ocean warming, eutrophication, overfishing, competitive exclusion and other biotic interactions \cite{Connell2010, Foster2010, Johnson2011, Wernberg2011, Wernberg2012, Moy2012, Falkenberg2012, Nystrom2012, Harley2012}. Some authors worryingly emphasize that the effect of any stressor on the kelp forest ecosystems will depend upon the presence and magnitude of other limiting or disruptive stressors \citep[see][]{Harley2012}. Anthropogenically forced changes in the environment tend to occur simultaneously, and synergism may therefore reinforce their impacts. The web of interacting mechanisms that reduce macrophyte communities may look different in different areas, and the holistic picture that emerges from around the globe is increasingly complex. 

\section{Drivers causing loss of kelp forests}
\label{sec:drivers}

Due to climate change, the average sea surface temperature has increased \cite{parry2007climate}. The geographical distribution of most marine algae is determined by water temperature \cite{VandenHoek1982, Luning1984, Hoek1988}, and ocean warming is expected to cause changes in the global distribution and abundance of macrophytes \cite{VandenHoek1990, Adey2001, Muller2009, Bartsch2012}. Temperature driven distribution changes have already been documented for inter-tidal seaweeds \cite{Wernberg2011a, Diez2012}, and to some extent also for kelps \cite{Serisawa2004, Johnson2011, Wernberg2012}. In addition to a general temperature increase, another important consequence of climate change is the higher frequency of extreme temperatures \cite{parry2007climate, Trenberth2012}, and these events can be detrimental to kelp forests \cite{Wernberg2012}. 

Climate change has also caused increases in precipitation and in the number freeze-thaw cycles, ultimately causing fresh-water run-off from land masses into coastal waters. The increased input of fresh-water, loaded with organic material, particles and nutrients, has reduced the transparency of the sea water in coastal areas (referred to as coastal darkening) \cite{Aksnes2009, Aksnes2009a}. The bursts of nutrients and organic matter from land masses stimulate production and algal blooms \citep[e.g.][]{Teichberg2012} which add to the darkening of the sea water and result in increased accumulation of sediments on the sea floor \cite{Cossellu2010}. The combined effects of high water temperature and reductions in water transparency may reduce the abundance and incidence of kelp \cite{Meleder2010}. Further, thick layers of sediments may hinder recovery by impairing spore attachment, increasing sand scour which detaches settled recruits, by blocking light, and by causing anoxic conditions that reduce the survival of recruits  \cite{Gorman2009, Schiel2006a, Deiman2012}.
% Søren: Du bør skrive hele avsnittet om, sådan at de ulike effekter (runoff, freeze/thaw etc.) først blir kort forklart. Deretter kan du vise at det hele drar i samme retning.

An alternative ecological system can become permanent, even if the pressure from the driver of the ecological shift were to be released (e.g. when the temperature goes from extreme high and back to normal), because new mechanisms that reinforce the change may be established \cite{Nystrom2012}. On several coasts where humans have altered the chemical, physical and biological conditions in the ocean through harvesting, land-use and pollution, canopies of kelp and seaweeds have been replaced by mats of turf-forming algae \cite{Eriksson2002, Connell2008, Moy2012}. While kelp canopies inhibit turfs \cite{Irving2006, Russell2007, Falkenberg2012}, reduced water quality resulting in more nutrients and less light may give the turf communities a competitive advantage and enable them to expand \cite{Gorman2009}. Subsequently, the cover of turf algae may hamper the recruitment of kelp and regeneration of kelp forests. Gorman and Connell \cite{Gorman2009} demonstrated a positive correlation between established turf carpets and sediment accumulation, that ultimately seemed to inhibit canopy recovery in kelp forest areas. %Furthermore, Connell and Russell \cite{Connell2010} showed that higher temperatures and increased oceanic uptake of CO\textsubscript{2} enhanced growth of turf algae. 
Deforested coastal areas may thus be the result of many different factors and cascading events.

\section{Kelp loss in Norway: \emph{Saccharina latissima}}
\label{sec:Norway}

\emph{Saccharina latissima} (Linnaeus) C. E. Lane, C. Mayes, Druehl \& G. W. Saunders (formerly known as \emph{Laminaria saccharina}) is a common forest founding kelp species in the North Atlantic. The species used to dominate most of the sub-tidal vegetation in sheltered areas along the Norwegian coast, but reports of \emph{S. latissima} forests in decline started to emerge the late 1990's and early 2000's. Later surveys \cite{Moy2006, Moy2008} showed that the forest deterioration had been extensive somewhere in between 1996 and 2002, but the lack of historic baseline data has rendered accurate assessments difficult. Estimates of losses range from 51 to 80\% \cite{Bekkby2011, Moy2012} along the Norwegian Skagerrak coast (approx. 7~900 km), while a loss of 40\% was estimated from the west coast up to M\o re and Romsdal county (approx. 26~000 km) \cite{Moy2012}. 

Moy and Christie \cite{Moy2012} offered multiple explanations for the onset and continuance of the forest deterioration, but the lack of empirical data unfortunately left proper testing of their hypotheses impossible. That being said, \emph{S. latissima} is considered a cold-temperate water species \cite{Wiencke1994}, and a couple of summers with extraordinarily high temperatures were considered the most likely initiator \cite{Moy2012}. The Norwegian surveys also showed that the kelp beds were replaced by turfs of ephemeral algae, suggesting that inhibition of forest recovery through competitive exclusion may have reinforced the demise.

While large kelp forest areas along the west coast of Norway recovered in cooler periods, most areas in Skagerrak remained devoid of kelp \cite{Moy2012}. The mechanisms responsible for the lack of regrowth in Skagerrak have been unknown. However, a substantial change in the vertical distribution of many macroalgae has occurred in Skagerrak, and reduced water transparency seems to have been an important driver \cite{Rueness1991, Pedersen2001, Eriksson2002}. The range of mechanisms and cascading events identified as drivers of increased loss and reduced kelp fitness elsewhere on the globe, thus, seem highly relevant in Norway as well. % Flere refs?

\section{Fitness and ecotypes}

Fitness is a central idea in evolutionary theory, and can be defined as a phenotypes ability to survive and reproduce in a given environment. %The phenotype is affected by the environment as well as by genes, and two genetically identical individuals may thus ``look and behave'' differently if they have been exposed to different environments. The British biologist J.B.S. Haldane was the first to quantify fitness in terms of the Darwinian evolutionary synthesis \cite{Haldane1927}, while the fitness landscape was initially conceptualized by Seawell Wright \cite{Wright1931}. Fitness landscapes are central concepts in the theory of adaptation, and describe the relationship between the success of phenotypes and the environment. Informally, the surface of a fitness landscape consists of phenotypes, where similar genotypes are close and the fitness values are presented as peaks. 
As the environment change, the combination of traits that are most likely to ensure the reproductive success and the survival of a species may also change. %The position of the peaks in the fitness landscape will then move accordingly, and adaptation can be pictured as a collective, population-level uphill walk in the fitness landscape over time. Continued loss of fitness in a population (a downhill walk) will ultimately lead to extinction, and faced with environmental change that leads to loss of fitness, a species must therefore either tolerate, acclimatize, adapt or migrate to persist. 

% Jeg syns hele avsnittet om fitness bør taes vekk. Du har introdusert mange økologiske konsepter (f.eks. cascading events) og t virker litt tilfeldig at du plutselig gir en så inngående gjennomgang  av ‘fitness’. Et eller annet sted bør du selvfølgelig (kort) nevne din ‘working definition’ av ‘fitnes’. Du velger sikkert at beholde avsnittet, men da bør du i den minnste begrunne hvorfor du kommer med en fitness-leksjon. 
% De som allerede kjenner til ‘fitness landscape’ kan ikke hente noe nytt her – og de som ikke kjenner til det kan ha vanskelig for å følge denne forklaring.

Genetic differentiation on a geographic scale typically occurs as populations adapt to a locally specific set of environmental conditions, and a geographically distinct variety of a species is often defined as an ecotype (\emph{sensu} Turesson \cite{Turesson1922}). Ecotypic differentiation is therefore expected in broadly distributed seaweed species \cite{Eggert2012}. \emph{Saccharina latissima} is a widely distributed species, known to comprise populations that differ both morphologically and physiologically \cite{Bartsch2008}, and ecotypic differentiation has been documented in relation to both thermal stress and light related responses within the species \cite{Gerard1988, Gerard1988a, Gerard1990, Gerard1997, J.2002, Liu2009}. The ability of \emph{S. latissima} to acclimate/acclimatize and tolerate environmental stress is therefore likely to depend on adaptations that vary geographically. Still, few investigations and comparisons of physiological traits in Norwegian populations have been published previous to the works included in this synthesis.


\section{The scope of the synthesis} 

The main objectives of the present thesis is to 1) evaluate the ability of \emph{Saccharina latissima} to grow, reproduce and survive in the water column in Skagerrak and 2) identify factors that reduce the fitness of Norwegian \emph{S. latissima}, and therefore are likely to obstruct recovery.
More specifically, for each paper the aim is to: 

\begin{description}
  \item[Paper I:] \hfill \\
  Document the seasonal patterns of \emph{S. latissima} growth, sporophyte fertility, epiphyte fouling and mortality in Skagerrak.
  \item[Paper II:] \hfill \\
  Investigate the seasonal and depth related patterns in growth responses, survival and epiphyte fouling in kelp from both Skagerrak and the west coast regions. Secondly, to investigate the shading properties of the epiphytes most commonly found on kelp fronds in Skagerrak. 
  \item[Paper III:] \hfill \\
  Investigate the seasonality in \emph{S. latissima} recruitment and the extent of coupling to sori development in the parent population. Secondly, to contribute with an evaluation of dispersal potentials and the potential for connectivity between \emph{S. latissima} populations in Skagerrak.
    \item[Paper IV:] \hfill \\
  Examine the ability in \emph{S. latissima} for temperature acclimation of photosynthesis and the level of tolerance in relation to high temperature. Secondly, we investigate whether kelp from different parts of a temperature gradient along the Norwegian coast may represent different temperature ecotypes.
\end{description}

% ---------------------------------------------------------------------- 
%\bibliography{/home/guri/Documents/PhD/Litteratur/bibtex/ThesisMS.bib}{}
%\bibliographystyle{plain}

%\end{document}