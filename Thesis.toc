\select@language {american}
\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {1.1}Drivers causing loss of kelp forests}{1}
\contentsline {section}{\numberline {1.2}Kelp loss in Norway: \emph {Saccharina latissima}}{2}
\contentsline {section}{\numberline {1.3}Fitness and ecotypes}{3}
\contentsline {section}{\numberline {1.4}The scope of the synthesis}{4}
\contentsline {chapter}{\numberline {2}Results and Discussion}{5}
\contentsline {section}{\numberline {2.1}Seasonal patterns}{5}
\contentsline {section}{\numberline {2.2}Depth related patterns -- kelp in a squeeze}{7}
\contentsline {section}{\numberline {2.3}Temperature and fitness}{8}
\contentsline {section}{\numberline {2.4}The impact of epiphyte covers}{9}
\contentsline {section}{\numberline {2.5}Ecotypic differentiation}{14}
\contentsline {section}{\numberline {2.6}A broader perspective}{14}
\contentsline {chapter}{\numberline {3}Overall assessment}{17}
\contentsline {section}{\numberline {3.1}Conclusions}{17}
\contentsline {section}{\numberline {3.2}Is natural recovery in Skagerrak still possible?}{17}
\contentsline {section}{\numberline {3.3}Where to go from here...}{18}
