\begin{thebibliography}{10}

\bibitem{Adey2001}
W.~H. Adey and R.~S. Steneck.
\newblock {Thermogeography over time creates biogeographic regions: a
  temperature/space/time-integrated model and an abundance-weighted test for
  benthic marine algae}.
\newblock {\em Journal of Phycology}, 37(5):677--698, Oct. 2001.

\bibitem{Aksnes2009}
D.~L. Aksnes, N.~Dupont, A.~Staby, S.~Kaartvedt, and J.~Aure.
\newblock {Coastal water darkening and implications for mesopelagic regime
  shifts in Norwegian fjords}.
\newblock {\em Marine Ecology Progress Series}, 387:39--49, 2009.

\bibitem{Aksnes2009a}
D.~L. Aksnes and M.~D. Ohman.
\newblock {Multi-decadal shoaling of the euphotic zone in the southern sector
  of the California Current System}.
\newblock {\em Limnology and Oceanography}, 54(4):1272--1281, 2009.

\bibitem{Bartsch2008}
I.~Bartsch, C.~Wiencke, K.~Bischof, C.~M. Buchholz, B.~H. Buck, A.~Eggert,
  P.~Feuerpfeil, D.~Hanelt, S.~Jacobsen, R.~Karez, U.~Karsten, M.~Molis, M.~Y.
  Roleda, H.~Schubert, R.~Schumann, K.~Valentin, F.~Weinberger, and J.~Wiese.
\newblock {The genus Laminaria sensu lato: recent insights and developments}.
\newblock {\em European Journal of Phycology}, 43(1):1--86, Sept. 2008.

\bibitem{Bartsch2012}
I.~Bartsch, C.~Wiencke, and T.~Laepple.
\newblock {Global Seaweed Biogeography Under a Changing Climate: The Prospected
  Effects of Temperature}.
\newblock In C.~Wiencke and K.~Bischof, editors, {\em Seaweed Biology},
  Ecological Studies, chapter~18, pages 383--406. Springer Berlin Heidelberg,
  Jan. 2012.

\bibitem{Bekkby2011}
T.~Bekkby and F.~Moy.
\newblock {Developing spatial models of sugar kelp (Saccharina latissima)
  potential distribution under natural conditions and areas of its
  disappearance in Skagerrak}.
\newblock {\em Estuarine, Coastal and Shelf Science}, 95(4):477--483, 2011.

\bibitem{J.2002}
J.~Borum, M.~Pedersen, D.~Krause-Jensen, P.~Christensen, and K.~Nielsen.
\newblock {Biomass, photosynthesis and growth of Laminaria saccharina in a
  high-arctic fjord, NE Greenland}.
\newblock {\em Marine Biology}, 141(1):11--19, July 2002.

\bibitem{Connell2010}
S.~Connell and B.~Russell.
\newblock {The direct effects of increasing CO2 and temperature on
  non-calcifying organisms: increasing the potential for phase shifts in kelp
  forests}.
\newblock {\em Proceedings of the Royal Society. Series B, Biological
  Sciences}, 277(1686):1409 --1415, May 2010.

\bibitem{2003}
S.~D. Connell.
\newblock {Negative Effects Overpower the Positive of Kelp to Exclude
  Invertebrates from the Understorey Community}.
\newblock {\em Oecologia}, 137(1):97--103, 2003.

\bibitem{Connell2008}
S.~D. Connell, B.~D. Russell, D.~J. Turner, A.~J.~S. Shepherd, T.~N. Kildea,
  D.~Miller, L.~Airoldi, and A.~Cheshire.
\newblock {Recovering a lost baseline: missing kelp forests from a metropolitan
  coast}.
\newblock {\em Marine Ecology Progress Series}, 360:63--72, 2008.

\bibitem{Cossellu2010}
M.~Cossellu and K.~Nordberg.
\newblock {Recent environmental changes and filamentous algal mats in shallow
  bays on the Swedish west coast - A result of climate change?}
\newblock {\em Journal of Sea Research}, 63(3-4):202--212, 2010.

\bibitem{Deiman2012}
M.~Deiman, K.~Iken, and B.~Konar.
\newblock {Susceptibility of Nereocystis luetkeana (Laminariales, Ochrophyta)
  and Eualaria fistulosa (Laminariales, Ochrophyta) spores to sedimentation}.
\newblock {\em Algae}, 27(2):115--123, 2012.

\bibitem{Diez2012}
I.~D\'{\i}ez, N.~Muguerza, a.~Santolaria, U.~Ganzedo, and J.~Gorostiaga.
\newblock {Seaweed assemblage changes in the eastern Cantabrian Sea and their
  potential relationship to climate change}.
\newblock {\em Estuarine, Coastal and Shelf Science}, 99:108--120, Mar. 2012.

\bibitem{Eggert2012}
A.~Eggert.
\newblock {Seaweed Responses to Temperature}.
\newblock In C.~Wiencke and K.~Bischof, editors, {\em Seaweed Biology}, volume
  219 of {\em Ecological Studies}, chapter~3, pages 47--66. Springer Berlin
  Heidelberg, Berlin, Heidelberg, 2012.

\bibitem{Eriksson2002}
B.~K. Eriksson, G.~Johansson, and P.~Snoeijs.
\newblock {Long‐term changes in the macroalgal vegetation of the inner
  Gullmar fjord, Swedish Skagerrak coast}.
\newblock {\em Journal of Phycology}, 38(2):284--296, Apr. 2002.

\bibitem{Falkenberg2012}
L.~Falkenberg, B.~Russell, and S.~Connell.
\newblock {Stability of strong species interactions resist the synergistic
  effects of local and global pollution in kelp forests}.
\newblock {\em PLoS ONE}, 7(3), Mar. 2012.

\bibitem{Fletcher1995}
R.~L. Fletcher.
\newblock {Epiphytism and fouling in Gracilaria cultivation: an overview}.
\newblock {\em Journal of Applied Phycology}, 7(3):325--333, May 1995.

\bibitem{Foster2010}
M.~S. Foster and D.~R. Schiel.
\newblock {Loss of predators and the collapse of southern California kelp
  forests (?): Alternatives, explanations and generalizations}.
\newblock {\em Journal of Experimental Marine Biology and Ecology},
  393(1-2):59--70, Sept. 2010.

\bibitem{Gerard1988}
V.~A. Gerard.
\newblock {Ecotypic differentiation in light-related traits of the kelp
  Laminaria saccharina}.
\newblock {\em Marine Biology}, 97(1):25--36, 1988.

\bibitem{Gerard1990}
V.~A. Gerard.
\newblock {Ecotypic differentiation in the kelp Laminaria saccharina:
  Phase-specific adaptation in a complex life cycle}.
\newblock {\em Marine Biology}, 107(3):519--528, Oct. 1990.

\bibitem{Gerard1997}
V.~A. Gerard.
\newblock {The role of nitrogen nutrition in high-temperature tolerance of the
  kelp Laminaria saccharina (Chromophyta)}.
\newblock {\em Journal of Phycology}, 33(5):800--810, 1997.

\bibitem{Gerard1988a}
V.~A. Gerard and K.~R. {Du Bois}.
\newblock {Temperature ecotypes near the southern boundary of the kelp
  Laminaria saccharina}.
\newblock {\em Marine Biology}, 97(4):575--580, Apr. 1988.

\bibitem{Gorman2009}
D.~Gorman and S.~D. Connell.
\newblock {Recovering subtidal forests in human-dominated landscapes}.
\newblock {\em Journal of Applied Ecology}, 46(6):1258--1265, 2009.

\bibitem{Hanelt2012}
D.~Hanelt and F.~{L\'{o}pez Figueroa}.
\newblock {Physiological and Photomorphogenic Effects of Light on Marine
  Macrophytes}.
\newblock In C.~Wiencke and K.~Bischof, editors, {\em Seaweed Biology}, volume
  219 of {\em Ecological Studies}, chapter~1, pages 3--23. Springer Berlin
  Heidelberg, Berlin, Heidelberg, 2012.

\bibitem{Harley2012}
C.~D.~G. Harley, K.~M. Anderson, K.~W. Demes, J.~P. Jorve, R.~L. Kordas, T.~A.
  Coyle, and M.~H. Graham.
\newblock {Effects of climate change on global seaweed communities}.
\newblock {\em Journal of Phycology}, 48(5):1064--1078, Oct. 2012.

\bibitem{Hepburn2005}
C.~Hepburn and C.~Hurd.
\newblock {Conditional mutualism between the giant kelp Macrocystis pyrifera
  and colonial epifauna}.
\newblock {\em Marine Ecology Progress Series}, 302:37--48, 2005.

\bibitem{Hepburn2006}
C.~Hepburn, C.~Hurd, and R.~Frew.
\newblock {Colony structure and seasonal differences in light and nitrogen
  modify the impact of sessile epifauna on the giant kelp Macrocystis pyrifera
  (L.) C Agardh}.
\newblock {\em Hydrobiologia}, 560(1):373--384, 2006.

\bibitem{Irving2006}
A.~Irving and S.~Connell.
\newblock {Physical disturbance by kelp abrades erect algae from the
  understorey}.
\newblock {\em Marine Ecology Progress Series}, 324:127--137, Oct. 2006.

\bibitem{Johnson2011}
C.~R. Johnson, S.~C. Banks, N.~S. Barrett, F.~Cazassus, P.~K. Dunstan, G.~J.
  Edgar, S.~D. Frusher, C.~Gardner, M.~Haddon, F.~Helidoniotis, K.~L. Hill,
  N.~J. Holbrook, G.~W. Hosie, P.~R. Last, S.~D. Ling, J.~Melbourne-Thomas,
  K.~Miller, G.~T. Pecl, A.~J. Richardson, K.~R. Ridgway, S.~R. Rintoul, D.~A.
  Ritz, D.~J. Ross, J.~C. Sanderson, S.~A. Shepherd, A.~Slotwinski, K.~M.
  Swadling, and N.~Taw.
\newblock {Climate change cascades: Shifts in oceanography, species' ranges and
  subtidal marine community dynamics in eastern Tasmania}.
\newblock {\em Journal of Experimental Marine Biology and Ecology},
  400(1-2):17--32, 2011.

\bibitem{Jones2000}
I.~J. Jones, J.~W. Eaton, and K.~Hardwick.
\newblock {The influence of periphyton on boundary layer conditions: a pH
  microelectrode investigation}.
\newblock {\em Aquatic Botany}, 67(3):191--206, July 2000.

\bibitem{Jormalainen2008}
V.~Jormalainen and T.~Honkanen.
\newblock {Macroalgal chemical defenses and their roles in structuring
  temperate marine communities}.
\newblock {\em Algal chemical ecology}, pages 57--89, 2008.

\bibitem{Liu2009}
F.~Liu and S.~J. Pang.
\newblock {Performances of growth, photochemical efficiency, and stress
  tolerance of young sporophytes from seven populations of Saccharina japonica
  (Phaeophyta) under short-term heat stress}.
\newblock {\em Journal of Applied Phycology}, 22:221--229, May 2009.

\bibitem{Luning1984}
K.~L\"{u}ning.
\newblock {Temperature tolerance and biogeography of seaweeds: The marine algal
  flora of Helgoland (North Sea) as an example}.
\newblock {\em Helgol\"{a}nder Meeresuntersuchungen}, 38(2):305--317, June
  1984.

\bibitem{Meleder2010}
V.~M\'{e}l\'{e}der, J.~Populus, B.~Guillaumont, T.~Perrot, and P.~Mouquet.
\newblock {Predictive modelling of seabed habitats: case study of subtidal kelp
  forests on the coast of Brittany, France}.
\newblock {\em Marine Biology}, 157(7):1525--1541, Apr. 2010.

\bibitem{Moy2008}
F.~E. Moy, H.~Chrisite, H.~Steen, P.~St{\aa}lnacke, D.~Aksnes, E.~Alve,
  J.~Aure, T.~Bekkby, S.~Fredriksen, J.~Gitmark, B.~Hackett, J.~Magnusson,
  A.~Pengerud, K.~Sj{\o}tun, K.~S{\o}rensen, L.~Tveiten, L.~{\O}ygarden, and
  P.~{\AA}sen.
\newblock {Sukkertareprosjektets sluttrapport}.
\newblock Technical report, NIVA, 2008.

\bibitem{Moy2012}
F.~E. Moy and H.~C. Christie.
\newblock {Large-scale shift from sugar kelp (Saccharina latissima) to
  ephemeral algae along the south and west coast of Norway}.
\newblock {\em Marine Biology Research}, 8(4):357--369, 2012.

\bibitem{Moy2006}
F.~E. Moy, H.~Steen, and H.~Christie.
\newblock {Redusert forekomst av sukkertare}.
\newblock Technical report, 2006.

\bibitem{Muller2009}
R.~M\"{u}ller, T.~Laepple, I.~Bartsch, and C.~Wiencke.
\newblock {Impact of oceanic warming on the distribution of seaweeds in polar
  and cold-temperate waters}.
\newblock {\em Botanica Marina}, 52(6):617--638, Jan. 2009.

\bibitem{Nystrom2012}
M.~Nystr\"{o}m, A.~V. Norstr\"{o}m, T.~Blenckner, M.~la~Torre-Castro, J.~S.
  Ekl\"{o}f, C.~Folke, H.~\"{O}sterblom, R.~S. Steneck, M.~Thyresson, and
  M.~Troell.
\newblock {Confronting Feedbacks of Degraded Marine Ecosystems}.
\newblock {\em Ecosystems}, 15(5):695--710, Mar. 2012.

\bibitem{parry2007climate}
M.~L. Parry, O.~Canziani, J.~Palutikof, P.~van~der Linden, and C.~Hanson.
\newblock {\em {Climate Change 2007: Impacts, Adaptation and Vulnerability:
  Working Group I Contribution to the Fourth Assessment Report of the IPCC}},
  volume~4.
\newblock Cambridge University Press, Cambridge, ar4 edition, 2007.

\bibitem{Pedersen2001}
M.~Peders\'{e}n and P.~Snoeijs.
\newblock {Patterns of macroalgal diversity, community composition and
  long-term changes along the Swedish west coast}.
\newblock {\em Hydrobiologia}, 459(1):83--102, 2001.

\bibitem{Potin2012}
P.~Potin.
\newblock {Intimate Associations Between Epiphytes, Endophytes, and Parasites
  of Seaweeds}.
\newblock In C.~Wiencke and K.~Bischof, editors, {\em Seaweed Biology}, volume
  219 of {\em Ecological Studies}, chapter~11, pages 203--234. Springer Berlin
  Heidelberg, Berlin, Heidelberg, 2012.

\bibitem{Rueness1991}
J.~Rueness and S.~Fredriksen.
\newblock {An assessment of possible pollution effects on the benthic algae of
  the outer Oslofjord, Norway}.
\newblock {\em Oebalia}, 17(1):223--235, 1991.

\bibitem{Russell2007}
B.~D. Russell.
\newblock {Effects of canopy-mediated abrasion and water flow on the early
  colonisation of turf-forming algae}.
\newblock {\em Marine and Freshwater Research}, 58(7):657--665, 2007.

\bibitem{Russell2005}
B.~D. Russell, T.~S. Elsdon, B.~M. Gillanders, and S.~D. Connell.
\newblock {Nutrients increase epiphyte loads: broad-scale observations and an
  experimental assessment}.
\newblock {\em Marine Biology}, 147(2):551--558, Feb. 2005.

\bibitem{Saunders2007}
M.~Saunders and A.~Metaxas.
\newblock {Temperature explains settlement patterns of the introduced bryozoan
  Membranipora membranacea in Nova Scotia, Canada}.
\newblock {\em Marine Ecology Progress Series}, 344:95--106, 2007.

\bibitem{Scheibling2009}
R.~E. Scheibling and P.~Gagnon.
\newblock {Temperature-mediated outbreak dynamics of the invasive bryozoan
  Membranipora membranacea in Nova Scotian kelp beds}.
\newblock {\em Marine Ecology Progress Series}, 390:1--13, 2009.

\bibitem{Schiel2006a}
D.~R. Schiel, S.~A. Wood, R.~A. Dunmore, and D.~I. Taylor.
\newblock {Sediment on rocky intertidal reefs: Effects on early post-settlement
  stages of habitat-forming seaweeds}.
\newblock {\em Journal of Experimental Marine Biology and Ecology},
  331(2):158--172, 2006.

\bibitem{Serisawa2004}
Y.~Serisawa, Z.~Imoto, T.~Ishikawa, and M.~Ohno.
\newblock {Decline of the Ecklonia cava population associated with increased
  seawater temperatures in Tosa Bay, southern Japan}.
\newblock {\em Fisheries Science}, 70(1):189--191, 2004.

\bibitem{Sjotun1993}
K.~Sj{\o}tun.
\newblock {Seasonal lamina growth in two age groups of Laminaria saccharina
  (L.) Lamour. in western Norway}.
\newblock {\em Botanica marina}, 36(5):433--442, Jan. 1993.

\bibitem{Steneck2002}
R.~S. Steneck, M.~H. Graham, B.~J. Bourque, D.~Corbett, J.~M. Erlandson, J.~A.
  Estes, and M.~J. Tegner.
\newblock {Kelp Forest Ecosystems: Biodiversity, Stability, Resilience and
  Future}.
\newblock {\em Environmental Conservation}, 29(4):436--459, 2002.

\bibitem{Teichberg2012}
M.~Teichberg, P.~Martinetto, and S.~E. Fox.
\newblock {Bottom-Up Versus Top-Down Control of Macroalgal Blooms}.
\newblock In C.~Wiencke and K.~Bischof, editors, {\em Seaweed Biology}, volume
  219 of {\em Ecological Studies}, chapter~21, pages 449--467. Springer Berlin
  Heidelberg, Berlin, Heidelberg, 2012.

\bibitem{Trannum2012}
H.~Trannum, K.~Norderhaug, L.~Naustvoll, B.~Bjerkeng, J.~Gitmark, and F.~Moy.
\newblock {Report for the Norwegian Monitoring Programme (KYS) 2011. Report
  6327 [In Norwegian with English summary]}.
\newblock Technical report, NIVA, Oslo, 2012.

\bibitem{Trenberth2012}
K.~E. Trenberth.
\newblock {Framing the way to relate climate extremes to climate change}.
\newblock {\em Climatic Change}, 115(2):283--290, Mar. 2012.

\bibitem{Turesson1922}
G.~Turesson.
\newblock {The Genotypical Response of the Plant Species to the Habitat}.
\newblock {\em Hereditas}, 3(3):211--350, 1922.

\bibitem{VandenHoek1982}
C.~van~den Hoek.
\newblock {The distribution of benthic marine algae in relation to the
  temperature regulation of their life histories}.
\newblock {\em Biological Journal of the Linnean Society}, 18(2):81--144, Sept.
  1982.

\bibitem{VandenHoek1990}
C.~van~den Hoek, A.~Breeman, and W.~Stam.
\newblock {The geographic distribution of seaweed species in relation to
  temperature: present and past}.
\newblock In {Jan J. Beukema}, W.~J. Wolff, and J.~J. W.~M. Brouns, editors,
  {\em Expected Effects of Climatic Change on Marine Coastal Ecosystems},
  volume~57, pages 55--67. Springer Netherlands, 1990.

\bibitem{Hoek1988}
C.~van~den Hoek and K.~L\"{u}ning.
\newblock {Biogeography of marine benthic algae}.
\newblock {\em Helgoland Marine Research}, 42(2):131--132, June 1988.

\bibitem{Waycott2009}
M.~Waycott, C.~M. Duarte, T.~J.~B. Carruthers, R.~J. Orth, W.~C. Dennison,
  S.~Olyarnik, A.~Calladine, J.~W. Fourqurean, K.~L. Heck, A.~R. Hughes, G.~A.
  Kendrick, W.~J. Kenworthy, F.~T. Short, and S.~L. Williams.
\newblock {Accelerating loss of seagrasses across the globe threatens coastal
  ecosystems}.
\newblock {\em Proceedings of the National Academy of Sciences of the United
  States of America}, 106(30):12377--12381, July 2009.

\bibitem{Wernberg2011}
T.~Wernberg, B.~Russell, and P.~Moore.
\newblock {Impacts of climate change in a global hotspot for temperate marine
  biodiversity and ocean warming}.
\newblock {\em Journal of Experimental Marine Biology and Ecology},
  400(1-2):7--16, Apr. 2011.

\bibitem{Wernberg2011a}
T.~Wernberg, B.~D. Russell, M.~S. Thomsen, C.~F.~D. Gurgel, C.~J. Bradshaw,
  E.~S. Poloczanska, and S.~D. Connell.
\newblock {Seaweed Communities in Retreat from Ocean Warming}.
\newblock {\em Current Biology}, 21(21):1828--1832, Nov. 2011.

\bibitem{Wernberg2012}
T.~Wernberg, D.~Smale, F.~Tuya, M.~S. Thomsen, T.~J. Langlois,
  T.~de~Bettignies, S.~Bennett, and C.~S. Rousseaux.
\newblock {An extreme climatic event alters marine ecosystem structure in a
  global biodiversity hotspot}.
\newblock {\em Nature Climate Change}, 3(1):78--82, Jan. 2013.

\bibitem{Wiencke1994}
C.~Wiencke, I.~Bartsch, B.~Bischoff, A.~F. Peters, and A.~M. Breeman.
\newblock {Temperature requirements and biogeography of Antarctic, Arctic and
  amphiequatorial seaweeds}.
\newblock {\em Botanica marina}, 37(3):247--259, 1994.

\bibitem{Worm2006}
B.~Worm and H.~K. Lotze.
\newblock {Effects of eutrophication, grazing, and algal blooms on rocky
  shores}.
\newblock {\em Limnology and Oceanography}, 51(1, part 2):569--579, 2006.

\end{thebibliography}
